function create_flash_table(json) {
    // Creating table of info with flashes

    // Creating headers of table
    let full_flash_table = "<div class=\"accordion\">" +
        "<table class=\"table table-hover\">\n" +
        "  <thead class=\"thead-dark\">\n" +
        "    <tr>\n" +
        "      <th scope=\"col\">Team</th>\n" +
        "      <th scope=\"col\">Name</th>\n" +
        "      <th scope=\"col\">All flashes</th>\n" +
        "      <th scope=\"col\">Successful flashes</th>\n" +
        "      <th scope=\"col\">Total duration</th>\n" +
        "    </tr>\n" +
        "  </thead>\n" +
        "  <tbody>\n"

    // Index to insert in id of collapsing elements
    let i = 1
    if (json.hasOwnProperty('flashes')) {
        const players = json['players']
        const rounds = 30

        // Remove CSGO player from info, probably should do in it in parser
        delete json.flashes[1]
        for (const player in json.flashes) {
            if (json.flashes.hasOwnProperty(player)) {
                let player_info = json.flashes[player]

                // Create row with info about player i
                full_flash_table += "    <tr data-toggle=\"collapse\" data-target=\"#player" + i + "_flashes\" aria-expanded=\"false\"" +
                    "      aria-controls='player" + i + "'>\n" +
                    "      <th scope=\"row\">" + players[player]['team'] + "</th>\n" +
                    "      <td>" + players[player]['name'] + "</td>\n" +
                    "      <td>" + player_info['flashes_amount'] + "</td>\n" +
                    "      <td>" + (player_info['successful_flashes_amount'] / player_info['flashes_amount'] * 100).toFixed(2) + "%</td>\n" +
                    "      <td>" + player_info['total_duration'].toFixed(2) + " s</td>\n" +
                    "    </tr>\n" +
                    "    <tr id='player" + i + "_flashes' class='collapse' style='background-color: unset !important; color: unset !important;'>\n" +
                    "      <td colspan='5'>\n"

                full_flash_table += "<table class=\"table table-borderless\" style='table-layout: fixed'>\n" +
                    "  <thead>\n" +
                    "    <tr style='background-color: unset !important; color: unset !important;'>"
                // Creating collapsing table to visualize grenades in each round
                for (let i = 1; i < 31; i++) {
                    full_flash_table += "<th scope=\"col\" class='text-center''>" + i + "</th>\n"
                }
                full_flash_table += "</tr>"
                full_flash_table += "</thead>"
                let flash_info = []
                for (let i = 0; i < rounds; i++) {
                    flash_info.push([])
                }
                if (player_info.hasOwnProperty("flashes")) {
                    for (const flash of player_info["flashes"]) {
                        if (flash["effective"] === true) {
                            flash_info[flash["g_round"] - 1].push(1)
                        } else {
                            flash_info[flash["g_round"] - 1].push(0)
                        }
                    }
                }
                let max_flashes = 0
                for (const arr in flash_info)
                    max_flashes = Math.max(max_flashes, arr.length)
                for (let i = 0; i < max_flashes; i++) {
                    full_flash_table += "<tr style='background-color: unset !important; color: unset !important;'>"
                    for (let k = 0; k < 30; k++) {
                        full_flash_table += "<td class='text-center'>"
                        if (flash_info[k].length > i)
                            if (flash_info[k][i] === 1)
                                full_flash_table += "<img alt='flash_hit_image' src=" + flashbang_hit_image + "></td>"
                            else
                                full_flash_table += "<img alt='flash_miss_image' src=" + flashbang_miss_image + "></td>"
                    }
                    full_flash_table += "</tr>"
                }
                full_flash_table += "</table>"
            }
            i++
        }
    }

    full_flash_table +=
        "  </tbody>\n" +
        "</table>\n" +
        "</div>\n";

    return full_flash_table
}

function create_grenade_table(json) {
    // Creating table of info with flashes
    let full_hes_table = "<div class=\"accordion\">" +
        "<table class=\"table table-hover\">\n" +
        "  <thead class=\"thead-dark\">\n" +
        "    <tr>\n" +
        "      <th scope=\"col\">Team</th>\n" +
        "      <th scope=\"col\">Name</th>\n" +
        "      <th scope=\"col\">All HE</th>\n" +
        "      <th scope=\"col\">Successful HE</th>\n" +
        "      <th scope=\"col\">Total damage</th>\n" +
        "    </tr>\n" +
        "  </thead>\n" +
        "  <tbody>\n"
    let i = 1
    if (json.hasOwnProperty('grenades')) {
        const players = json['players']
        const rounds = 30
        delete json.grenades[1]
        for (const player in json.grenades) {
            if (json.grenades.hasOwnProperty(player)) {
                let player_info = json.grenades[player]
                full_hes_table += "    <tr data-toggle=\"collapse\" data-target=\"#player" + i + "_grenades\" aria-expanded=\"false\"" +
                    "      aria-controls='player" + i + "'>\n" +
                    "      <th scope=\"row\">" + players[player]['team'] + "</th>\n" +
                    "      <td>" + players[player]['name'] + "</td>\n" +
                    "      <td>" + player_info['grenades_amount'] + "</td>\n" +
                    "      <td>" + (player_info['successful_grenades_amount'] / player_info['grenades_amount'] * 100).toFixed(2) + "%</td>\n" +
                    "      <td>" + player_info['total_damage'].toFixed(2) + "</td>\n" +
                    "    </tr>\n" +
                    "    <tr id='player" + i + "_grenades' class='collapse' style='background-color: unset !important; color: unset !important;'>\n" +
                    "      <td colspan='5'>\n"
                full_hes_table += "<table class=\"table table-borderless\" style='table-layout: fixed'>\n" +
                    "      <thead>\n" +
                    "    <tr style='background-color: unset !important; color: unset !important;'>"
                for (let i = 1; i < 31; i++) {
                    full_hes_table += "<th scope=\"col\" class='text-center''>" + i + "</th>\n"
                }
                full_hes_table += "</tr>"
                full_hes_table += "</thead>"
                let nades_info = []
                for (let i = 0; i < rounds; i++) {
                    nades_info.push([])
                }
                if (player_info.hasOwnProperty("grenades")) {
                    for (const grenade of player_info["grenades"]) {

                        if (grenade["effective"] === true) {
                            nades_info[grenade["g_round"] - 1].push(1)
                        } else {
                            nades_info[grenade["g_round"] - 1].push(0)
                        }
                    }
                }
                let max_grenades = 0
                for (const arr in nades_info)
                    max_grenades = Math.max(max_grenades, arr.length)
                for (let i = 0; i < max_grenades; i++) {
                    full_hes_table += "<tr style='background-color: unset !important; color: unset !important;'>"
                    for (let k = 0; k < 30; k++) {
                        full_hes_table += "<td class='text-center'>"
                        if (nades_info[k].length > i)
                            if (nades_info[k][i] === 1)
                                full_hes_table += "<img style='width: 27px; height: 27px;' src=" + grenade_hit_image + "></td>"
                            else
                                full_hes_table += "<img style='width: 27px; height: 27px;' src=" + grenade_miss_image + "></td>"
                    }
                    full_hes_table += "</tr>"
                }
                full_hes_table += "</table>"
            }
            i++
        }
    }
    full_hes_table +=
        "  </tbody>\n" +
        "</table>\n" +
        "</div>\n";
    return full_hes_table
}

function create_table_total_stats(json) {
    let full_stats_table = "<div class=\"accordion\">" +
        "<table class=\"table table-hover\">\n" +
        "  <thead class=\"thead-dark\">\n" +
        "    <tr>\n" +
        "      <th scope=\"col\">Team</th>\n" +
        "      <th scope=\"col\">Name</th>\n" +
        "      <th scope=\"col\">Kill</th>\n" +
        "      <th scope=\"col\">Assists</th>\n" +
        "      <th scope=\"col\">Deaths</th>\n" +
        "      <th scope=\"col\">ADR</th>\n" +
        "      <th scope=\"col\">K/D ratio</th>\n" +
        "      <th scope=\"col\">HLTV 2.0</th>\n" +
        "    </tr>\n" +
        "  </thead>\n" +
        "  <tbody>\n"
    let i = 1

    if (json.hasOwnProperty('stats')) {
        const players = json['players']
        delete json.stats[1]
        for (const player in json.stats) {
            if (json.stats.hasOwnProperty(player)) {
                let player_info = json.stats[player]
                full_stats_table += "    <tr data-toggle=\"collapse\" data-target=\"#player" + i + "_flashes\" aria-expanded=\"false\"" +
                    "      aria-controls='player" + i + "'>\n" +
                    "      <th scope=\"row\">" + players[player]['team'] + "</th>\n" +
                    "      <td>" + players[player]['name'] + "</td>\n" +
                    "      <td>" + player_info['kills'] + "</td>\n" +
                    "      <td>" + player_info['assists'] + "</td>\n" +
                    "      <td>" + player_info['deaths'] + "</td>\n" +
                    "      <td>" + player_info['adr'].toFixed(2) + "</td>\n" +
                    "      <td>" + (player_info['kills'] / player_info['deaths']).toFixed(2) + "</td>\n" +
                    "      <td>" + player_info['hltv_rating'].toFixed(2) + "</td>\n" +
                    "    </tr>\n" +
                    "    <tr id='player" + i + "_flashes' class='collapse' style='background-color: unset !important; color: unset !important;'>\n" +
                    "      <td colspan='8'>\n"

                full_stats_table += "<table class=\"table table-borderless\" style='table-layout: fixed'>\n" +
                    "    <tr style='background-color: unset !important; color: unset !important;'>\n" +
                    "      <td scope=\"col\" class='text-center''>KAST: " + player_info['kast'].toFixed(2) + "%</th>\n" +
                    "      <td scope=\"col\" class='text-center''>1 kill rounds: " + player_info['1k'] + "</th>\n" +
                    "    </tr>\n" +
                    "    <tr style='background-color: unset !important; color: unset !important;'>\n" +
                    "      <td scope=\"col\" class='text-center''>2 kills rounds: " + player_info['2k'] + "</th>\n" +
                    "      <td scope=\"col\" class='text-center''>3 kills rounds: " + player_info['3k'] + "</th>\n" +
                    "    </tr>\n" +
                    "    <tr style='background-color: unset !important; color: unset !important;'>\n" +
                    "      <td scope=\"col\" class='text-center''>4 kills rounds: " + player_info['4k'] + "</th>\n" +
                    "      <td scope=\"col\" class='text-center''>5 kills rounds: " + player_info['5k'] + "</th>\n" +
                    "    </tr>\n" +
                    "    <tr style='background-color: unset !important; color: unset !important;'>\n" +
                    "      <td scope=\"col\" class='text-center''>Open frags: " + player_info['f_blood'] + "</th>\n" +
                    "      <td scope=\"col\" class='text-center''>First deaths: " + player_info['f_death'] + "</th>\n" +
                    "    </tr>\n"

                if (json.hasOwnProperty('stupid_deaths')) {
                    let deaths_amount = json['stupid_deaths'][player]
                    full_stats_table += "<tr style='background-color: unset !important; color: unset !important;'>\n" +
                        "      <td colspan='2' class='text-center''>Deaths with knife: " + deaths_amount + "</th>\n" +
                        "    </tr>\n"
                }

                full_stats_table += "</tr>\n" +
                    "</table>\n"
            }
            i++
        }
    }
    full_stats_table +=
        "  </tbody>\n" +
        "</table>\n" +
        "</div>\n";
    return full_stats_table
}
