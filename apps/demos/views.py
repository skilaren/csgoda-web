import subprocess

from django.shortcuts import render
from django.http import JsonResponse
from django.views import View
from django.core.files.storage import default_storage

from apps.demos.parser.parser import DemoParser


class DemoView(View):

    def get(self, request):
        return render(request, template_name='demos/upload_demo_page.html', context={})

    def post(self, request):
        file = request.FILES['demoFile']
        file_name = default_storage.save(file.name, file)
        context = {
            'file_name': file_name
        }
        return render(request, template_name='demos/analyzed_demo_page.html', context=context)


class DemoAnalysis(View):

    def get(self, request):
        file_name = request.GET['file_name']
        out_file_name = f'{file_name.split(".")[0]}'
        path = default_storage.path(file_name)
        subprocess.run(['./parser', '-demo', path,
                        '-format', 'json',
                        '-freq', '8',
                        '-out', out_file_name])

        result = DemoParser.parse(out_file_name)
        if file_name != 'trial_demo_to_demo.dem':
            default_storage.delete(file_name)
            default_storage.delete(out_file_name)
        return JsonResponse(result)


class IndexPage(View):

    def get(self, request):
        return render(request, template_name='demos/index.html', context={})

    def post(self, request):
        context = {
            'file_name': 'trial_demo_to_demo.dem'
        }
        return render(request, template_name='demos/analyzed_demo_page.html', context=context)