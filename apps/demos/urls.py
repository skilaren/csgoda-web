from django.urls import path
from .views import DemoView, DemoAnalysis, IndexPage

urlpatterns = [
    path('', IndexPage.as_view(), name='index_page_url'),
    path('demos/', DemoView.as_view(), name='demo_base_url'),
    path('demos/analyze/', DemoAnalysis.as_view(), name='demo_analyze_url'),
]
